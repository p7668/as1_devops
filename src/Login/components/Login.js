import React from 'react'
import '../styles/styles.css'

export default class Login extends React.Component {
    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            labelValue: ''
        }
    }

    handleSubmit = (e) => {
        e.preventDefault(); // Pra não recarregar a página quando eu clicar no botão "Acessar"

        if (this.state.email === 'eduardo.lino@pucpr.br' && this.state.password === '123456') this.setState({
            ...this.state,
            labelValue: 'Acessado com sucesso!'
        })
        else this.setState({
            ...this.state,
            labelValue: 'Usuário ou senha incorretos!'
        })
    }

    updateFormValue = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div className="main-container">

                <div className='content-container'>
                    <h1>Login</h1>

                    <div className='inputs-container'>
                        <form onSubmit={this.handleSubmit}>
                            <input type="email" name="email" placeholder="E-mail" onChange={this.updateFormValue} />
                            <input type="password" name="password" placeholder="Senha" onChange={this.updateFormValue} />

                            <button type="submit">Acessar</button>
                        </form>
                    </div>

                    <label>{this.state.labelValue}</label>
                </div>

            </div>
        )
    }
}